# Print IP and Visitor Count

## Overview

This repository contain a simple php script that will print the user IP and also visitor counter.

## Prerequisites

We assume that you have docker installation running on your system.

## How To Build

```
docker build -t print-ip .
```

## How To Run

```
docker run -dit --name print-ip -p 80:80 print-ip
```

## How To Test

```
curl localhost/
```